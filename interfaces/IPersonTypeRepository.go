package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IPersonTypeRepository interface {
	GetPersonTypeById(id int) (models.PersonTypeModel, error)
}
