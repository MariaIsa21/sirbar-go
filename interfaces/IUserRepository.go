package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IUserRepository interface {
	GetUserById(id int) (models.UserModel, error)
}
