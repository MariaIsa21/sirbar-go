package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IPasswordRepository interface {
	GetPasswordById(id int) (models.PasswordModel, error)
}
