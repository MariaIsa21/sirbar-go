package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type ICountryRepository interface {
	GetCountryById(id int) (models.CountryModel, error)
}
