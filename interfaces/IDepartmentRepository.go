package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IDepartmentRepository interface {
	GetDepartmentById(id int) (models.DepartmentModel, error)
}
