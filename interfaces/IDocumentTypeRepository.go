package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IDocumentTypeRepository interface {
	GetDocumentTypeById(id int) (models.DocumentTypeModel, error)
}
