package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IProductRepository interface {
	GetProductById(id int) (models.ProductModel, error)
}
