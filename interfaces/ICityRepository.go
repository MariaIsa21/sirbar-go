package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type ICityRepository interface {
	GetCityById(id int) (models.CityModel, error)
}
