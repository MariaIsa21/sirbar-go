package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IPersonRepository interface {
	GetPersonById(id int) (models.PersonModel, error)
}
