package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IRolRepository interface {
	GetRolById(id int) (models.RolModel, error)
}
