package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type IGenderRepository interface {
	GetGenderById(id int) (models.GenderModel, error)
}
