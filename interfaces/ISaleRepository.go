package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type ISaleRepository interface {
	GetSaleById(id int) (models.SaleModel, error)
}
