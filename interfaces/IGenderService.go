package interfaces

type IGenderService interface {
	GetGender() (string, error)
}
