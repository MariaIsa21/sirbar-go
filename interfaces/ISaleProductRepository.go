package interfaces

import "bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

type ISaleProductRepository interface {
	GetSaleProductById(id int) (models.SaleProductModel, error)
	GetSaleProductByIdSale(idSale int) (models.SaleProductModel, error)
}
