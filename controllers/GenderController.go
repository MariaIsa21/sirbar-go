package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/MariaIsa21/sirbar-go/src/master/interfaces"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/models"
)

type GenderController struct {
	interfaces.IGenderService
}

func (controller *GenderController) GetGenderC(res http.ResponseWriter, req *http.Request) {
	gender, err := controller.GetGender()
	if err != nil {
	}

	res.Header().Set("Content-Type", "application/json")
	var genderModel models.GenderModel
	genderModel.Id = 1
	genderModel.Description = gender
	genderModel.Symbol = ""
	genderModel.State = true

	json.NewEncoder(res).Encode(genderModel)
}
