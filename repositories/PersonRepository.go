package repositories

import (
	"fmt"
	"time"

	"bitbucket.org/MariaIsa21/sirbar-go/src/master/interfaces"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/models"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/utils"

	"github.com/afex/hystrix-go/hystrix"
)

type PersonRepositoryWithCircuitBreaker struct {
	PersonRepository interfaces.IPersonRepository
}

func (repository *PersonRepositoryWithCircuitBreaker) GetPersonById(id int) (models.PersonModel, error) {
	output := make(chan models.PersonModel, 1)
	hystrix.ConfigureCommand("get_person_by_id", hystrix.CommandConfig{Timeout: 1000})
	errors := hystrix.Go("get_person_by_id", func() error {
		player, _ := repository.PersonRepository.GetPersonById(id)
		output <- player
		return nil
	}, nil)

	select {
	case out := <-output:
		return out, nil
	case err := <-errors:
		println(err)
		return models.PersonModel{}, err
	}
}

type PersonRepository struct {
	interfaces.IDbHandler
}

func (repository *PersonRepository) GetPersonById(id int) (models.PersonModel, error) {
	row, err := repository.Query(fmt.Sprintf("SELECT person_id, name, document_type_id,"+
		" person_type_id, birthdate, gender_id, email_address, phone_number, city_id, address, "+
		"create_date, modification_date, state FROM person WHERE person_id = %d", id))
	if err != nil {
		return models.PersonModel{}, err
	}
	var person models.PersonModel
	var idDocumentType, idPersonType, idGender int
	var birthdate string
	row.Next()
	row.Scan(&person.Id, &person.Name, &idDocumentType, &idPersonType, &birthdate, &idGender)
	var documentTypeRepository interfaces.IDocumentTypeRepository
	person.DocumentType, err = documentTypeRepository.GetDocumentTypeById(idDocumentType)
	var personTypeRepository interfaces.IPersonTypeRepository
	person.PersonType, err = personTypeRepository.GetPersonTypeById(idPersonType)
	person.Birthdate, err = time.Parse(utils.LayoutDate, birthdate)
	var genderRepository interfaces.IGenderRepository
	person.Gender, err = genderRepository.GetGenderById(idGender)
	return person, nil
}
