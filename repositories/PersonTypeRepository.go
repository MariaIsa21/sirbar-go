package repositories

import (
	"fmt"

	"bitbucket.org/MariaIsa21/sirbar-go/src/master/interfaces"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

	"github.com/afex/hystrix-go/hystrix"
)

type PersonTypeRepositoryWithCircuitBreaker struct {
	PersonTypeRepository interfaces.IPersonTypeRepository
}

func (repository *PersonTypeRepositoryWithCircuitBreaker) GetPersonTypeById(id int) (models.PersonTypeModel, error) {
	output := make(chan models.PersonTypeModel, 1)
	hystrix.ConfigureCommand("get_person_type_by_id", hystrix.CommandConfig{Timeout: 1000})
	errors := hystrix.Go("get_person_type_by_id", func() error {
		personType, _ := repository.PersonTypeRepository.GetPersonTypeById(id)
		output <- personType
		return nil
	}, nil)

	select {
	case out := <-output:
		return out, nil
	case err := <-errors:
		println(err)
		return models.PersonTypeModel{}, err
	}
}

type PersonTypeRepository struct {
	interfaces.IDbHandler
}

func (repository *PersonTypeRepository) GetPersonTypeById(id int) (models.PersonTypeModel, error) {
	row, err := repository.Query(fmt.Sprintf("SELECT person_type_id, description, state "+
		"FROM person_type WHERE person_type_id = %d", id))
	if err != nil {
		return models.PersonTypeModel{}, err
	}
	var personType models.PersonTypeModel
	row.Next()
	row.Scan(&personType.Id, &personType.Description, &personType.State)
	return personType, nil
}
