package repositories

import (
	"fmt"

	"bitbucket.org/MariaIsa21/sirbar-go/src/master/interfaces"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

	"github.com/afex/hystrix-go/hystrix"
)

type GenderRepositoryWithCircuitBreaker struct {
	GenderRepository interfaces.IGenderRepository
}

func (repository *GenderRepositoryWithCircuitBreaker) GetGenderById(id int) (models.GenderModel, error) {
	output := make(chan models.GenderModel, 1)
	hystrix.ConfigureCommand("get_gender_by_id", hystrix.CommandConfig{Timeout: 1000})
	errors := hystrix.Go("get_gender_by_id", func() error {
		gender, _ := repository.GenderRepository.GetGenderById(id)
		output <- gender
		return nil
	}, nil)
	select {
	case out := <-output:
		return out, nil
	case err := <-errors:
		println(err)
		return models.GenderModel{}, err
	}
}

type GenderRepository struct {
	interfaces.IDbHandler
}

func (repository *GenderRepository) GetGenderById(id int) (models.GenderModel, error) {
	row, err := repository.Query(fmt.Sprintf("SELECT gender_id, description, "+
		"symbol, state FROM gender WHERE gender_id = %d", id))
	if err != nil {
		return models.GenderModel{}, err
	}
	var gender models.GenderModel
	row.Scan(&gender.Id, &gender.Description, &gender.Symbol, &gender.State)
	return gender, nil
}
