package repositories

import (
	"fmt"

	"bitbucket.org/MariaIsa21/sirbar-go/src/master/interfaces"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/models"

	"github.com/afex/hystrix-go/hystrix"
)

type DocumentTypeRepositoryWithCircuitBreaker struct {
	DocumentTypeRepository interfaces.IDocumentTypeRepository
}

func (repository *DocumentTypeRepositoryWithCircuitBreaker) GetDocumentTypeById(id int) (models.DocumentTypeModel, error) {
	output := make(chan models.DocumentTypeModel, 1)
	hystrix.ConfigureCommand("get_document_type_by_id", hystrix.CommandConfig{Timeout: 1000})
	errors := hystrix.Go("get_document_type_by_id", func() error {
		documentType, _ := repository.DocumentTypeRepository.GetDocumentTypeById(id)
		output <- documentType
		return nil
	}, nil)

	select {
	case out := <-output:
		return out, nil
	case err := <-errors:
		println(err)
		return models.DocumentTypeModel{}, err
	}
}

type DocumentTypeRepository struct {
	interfaces.IDbHandler
}

func (repository *DocumentTypeRepository) GetDocumentTypeById(id int) (models.DocumentTypeModel, error) {
	row, err := repository.Query(fmt.Sprintf("SELECT document_type_id, description, "+
		"state FROM document_type WHERE document_type_id = %d", id))
	if err != nil {
		return models.DocumentTypeModel{}, err
	}
	var documentType models.DocumentTypeModel
	row.Next()
	row.Scan(&documentType.Id, &documentType.Description, &documentType.State)
	return documentType, nil
}
