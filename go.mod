module bitbucket.org/MariaIsa21/sirbar-go/src/master

go 1.16

require (
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/go-chi/chi v1.5.4
	github.com/go-sql-driver/mysql v1.6.0
)
