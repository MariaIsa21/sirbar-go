package infrastructures

import (
	"database/sql"
	"fmt"

	"bitbucket.org/MariaIsa21/sirbar-go/src/master/interfaces"
)

type MySqlHandler struct {
	Conn *sql.DB
}

func (handler *MySqlHandler) Execute(statement string) {
	handler.Conn.Exec(statement)
}

func (handler *MySqlHandler) Query(statement string) (interfaces.IRow, error) {
	//fmt.Println(statement)
	rows, err := handler.Conn.Query(statement)

	if err != nil {
		fmt.Println()
		return new(MySqlRow), err
	}
	row := new(MySqlRow)
	row.Rows = rows

	return row, nil
}

type MySqlRow struct {
	Rows *sql.Rows
}

func (r MySqlRow) Scan(dest ...interface{}) error {
	err := r.Rows.Scan(dest...)
	if err != nil {
		return err
	}
	return nil
}

func (r MySqlRow) Next() bool {
	return r.Rows.Next()
}
