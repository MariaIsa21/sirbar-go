package models

type CountryModel struct {
	Id    int
	Name  string
	State bool
}
