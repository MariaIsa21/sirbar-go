package models

type SaleProductModel struct {
	Id       int
	Sale     SaleModel
	Product  ProductModel
	Quantity int
	State    bool
}
