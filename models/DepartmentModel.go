package models

type DepartmentModel struct {
	Id      int
	Name    string
	Country CountryModel
	State   bool
}
