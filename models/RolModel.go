package models

type RolModel struct {
	Id          int
	Description string
	State       bool
}
