package models

import "time"

type SaleModel struct {
	Id               int
	Seller           UserModel
	Customer         UserModel
	TotalPrice       float64
	CreateDate       time.Time
	ModificationDate time.Time
	State            bool
}
