package models

type DocumentTypeModel struct {
	Id          int
	Description string
	State       bool
}
