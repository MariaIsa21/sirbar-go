package models

import "time"

type PersonModel struct {
	Id               int
	Name             string
	DocumentType     DocumentTypeModel
	PersonType       PersonTypeModel
	Birthdate        time.Time
	Gender           GenderModel
	EmailAddress     string
	Phone_number     int
	City             CityModel
	Address          string
	CreateDate       time.Time
	ModificationDate time.Time
	State            bool
}
