package models

type CityModel struct {
	Id         int
	Name       string
	Department DepartmentModel
	State      bool
}
