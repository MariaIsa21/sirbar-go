package models

type UserModel struct {
	Id     int
	Person PersonModel
	Rol    RolModel
	State  bool
}
