package models

type GenderModel struct {
	Id          int
	Description string
	Symbol      string
	State       bool
}
