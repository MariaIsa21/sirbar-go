package models

import "time"

type PasswordModel struct {
	Id               int
	User             UserModel
	EncryptPassword  string
	CreateDate       time.Time
	ModificationDate time.Time
	State            bool
}
