package models

type ProductModel struct {
	Id          int
	Description string
	Quantity    int
	Price       float64
	State       bool
}
