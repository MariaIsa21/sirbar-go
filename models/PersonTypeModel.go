package models

type PersonTypeModel struct {
	Id          int
	Description string
	State       bool
}
