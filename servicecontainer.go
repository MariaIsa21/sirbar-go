package main

import (
	"database/sql"
	"sync"

	"bitbucket.org/MariaIsa21/sirbar-go/src/master/controllers"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/infrastructures"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/repositories"
	"bitbucket.org/MariaIsa21/sirbar-go/src/master/services"
	//"github.com/go-sql-driver/mysql"
)

type IServiceContainer interface {
	InjectGenderController() controllers.GenderController
}

type kernel struct{}

func (k *kernel) InjectGenderController() controllers.GenderController {
	sqlConn, _ := sql.Open("mysql", "sirbaradmin:s1rb4R!2021@tcp(127.0.0.1:3306)/sirbar_db")
	mysqlHandler := &infrastructures.MySqlHandler{}
	mysqlHandler.Conn = sqlConn

	genderRepository := &repositories.GenderRepository{mysqlHandler}
	genderService := &services.GenderService{&repositories.GenderRepositoryWithCircuitBreaker{genderRepository}}
	genderController := controllers.GenderController{genderService}

	return genderController
}

var (
	k             *kernel
	containerOnce sync.Once
)

func ServiceContainer() IServiceContainer {
	if k == nil {
		containerOnce.Do(func() {
			k = &kernel{}
		})
	}
	return k
}
